//
class nav extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="active" href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
///////////////////////////////////////////////////////////////////////////////
class nav1 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a class="active" href="agenda.html">Agenda</a></li>
          <li><a href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
               <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
class nav2 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a class="active" href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
class nav3 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a  href="services.html">Services</a></li>
          <li class="dropdown"><a class="active" href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
               <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
class nav4 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a  href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
               <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a class="active" href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}

class nav5 extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
   
    <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!--  <h1 class="logo me-auto"><a href="index.html">RDO</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
     <a href="index.html" class="logo me-auto"><img src="assets/img/rdo_logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a  href="index.html">Home</a></li>
          <li><a  href="agenda.html">Agenda</a></li>
          <li><a  href="services.html">Services</a></li>
          <li class="dropdown"><a href="productivity.html"><span>Research Productivity</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
               <li class="dropdown"><a href="publication.html"><span>Research Publications</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="journal.html">Journal of Education and Society</a></li>
                </ul>
              </li>
              <li class="dropdown"><a href="#"><span>Research Projects</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Institutional</a></li>
                  <li><a href="#">Externally Funded</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="faculty.html">Faculty Research Involvement</a></li>
          <li><a href="centers.html">Research Centers</a></li>
          <li><a href="highlights.html">Highlights of 2022</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      

    </div>
  </header><!-- End Header -->

    `;
  }
}
customElements.define('main-nav', nav);
customElements.define('main-nav1', nav1);
customElements.define('main-nav2', nav2);
customElements.define('main-nav3', nav3);
customElements.define('main-nav4', nav4);
customElements.define('main-nav5', nav5);
